
export const onTaskNameAction = (value) => {
    return {
        type:"INPUT_TASK_NAME",
        payload:value
    }
}

export const addTaskNameAction = () => {
    return {
        type:"ADD_TASK_NAME"
    }
}

export const changeTaskStatusAction = (index) => {
    return {
        type:"CHANGE_TASK_STATUS",
        payload:index
    }
}
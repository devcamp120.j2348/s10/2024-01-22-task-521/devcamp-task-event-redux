import { combineReducers } from "redux";
import taskReducer from "./task.reducer";

const rootReducer = combineReducers({
    taskReducer,
    //danh sach cac reducer khac
})

export default rootReducer;
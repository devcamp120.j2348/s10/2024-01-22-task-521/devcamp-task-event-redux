
//Khoi tao state
/*
const initialState = {
    inputString:"Task 123",
    taskList:[{
        taskName:"Task 1",
        status:false
    },
    {
        taskName:"Task 2",
        status:true
    }]
}*/
const initialState = {
    inputString:"",
    taskList:[]
}

//khai bao reducer
const taskReducer = (state = initialState, action) => {
    console.log(action);
    switch (action.type) {
        case "INPUT_TASK_NAME":
            state.inputString = action.payload;
            break;
        
        case "ADD_TASK_NAME":
            state.taskList.push({
                taskName:state.inputString,
                status:false
            });
            state.inputString = "";
            break;

        case "CHANGE_TASK_STATUS":
            const index = action.payload;
            state.taskList[index].status = !state.taskList[index].status;
            break
        default:
            break;    
    }
    return {...state};
}

export default taskReducer;
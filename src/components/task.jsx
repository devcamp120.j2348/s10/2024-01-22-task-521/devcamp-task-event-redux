import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { addTaskNameAction, changeTaskStatusAction, onTaskNameAction } from "../actions/task.action";

const Task = () => {
    //khai bao ham gui action den reducer
    const dispatch = useDispatch();

    //doc state tu store
    const {inputString, taskList} = useSelector((reduxData) => {
        console.log(reduxData);
        return reduxData.taskReducer;
    })
    
    const onTaskNameChangeHandler = (event) => {
        dispatch(onTaskNameAction(event.target.value));
    }

    const onAddTaskClickHandler = () => {
        dispatch(addTaskNameAction());
    }

    const onTaskItemClickHander = (index) => {
        dispatch(changeTaskStatusAction(index));
    }
    return (
        <Container>
            {/* input task */}
            <Grid container spacing={2} mt={5} alignItems="center">
                <Grid item md={9}>
                    <TextField id="task-name" fullWidth label="Task Name" value={inputString} onChange={onTaskNameChangeHandler} variant="standard" placeholder="Nhap task name" />
                </Grid>
                <Grid item md={3}>
                    <Button variant="contained" onClick={onAddTaskClickHandler}>Add Task</Button>
                </Grid>
            </Grid>
            {/* list task */}
            <Grid container>
                <List>
                    {
                        taskList.map((element,index) => {
                            return <ListItem key={index} style={{color:element.status ? "red" : "green"}} onClick={()=>onTaskItemClickHander(index)}>
                                        {index+1}. {element.taskName}
                                   </ListItem>
                        })
                    }
                </List>
            </Grid>
        </Container>
    )
}

export default Task;